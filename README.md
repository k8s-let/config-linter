# config-linter

Prepacked executable of [config-linter](https://github.com/stelligent/config-lint/) in a docker image. 

Used in Gitlab CI/CD as

```
script:
  - config-lint -rules /usr/local/share/rules/argocd-labels.yml <directory to check recursively>
```

More rule files can be applied, see `config-lint --help`
